# composite-dhall

`ToDhall` and `FromDhall` instances for
[composite](https://hackage.haskell.org/package/composite-base) records.
