# Changelog for composite-dhall

## v0.0.1.0

* Add `ToDhall` and `FromDhall` instances for composite `Record`s.
